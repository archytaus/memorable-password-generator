#![feature(alloc_system)]
extern crate alloc_system;

extern crate rand;

use rand::{thread_rng, Rng};
use std::env;
use std::str::FromStr;

const WORDS: &'static str = include_str!("words.txt");
const DEFAULT_WORD_COUNT: usize = 5;
const DEFAULT_PASSWORD_COUNT: usize = 5;

fn generate_password<R: Rng>(word_list: &Vec<&str>, rand: &mut R, count: usize) -> String {
    let password_parts: Vec<_> = (0..)
                    .map(|_| *rand.choose(&word_list).unwrap())
                    .take(count)
                    .collect();

    return password_parts.as_slice().join("-");
}

fn parse_argument(argument: String) -> Option<usize> {
    match usize::from_str(&argument) {
        Ok(parsed_argument) => {
            Some(parsed_argument)
        },
        Err(err) => {
            println!("Failed to parse '{}'. Reason: {}", argument, err);
            None
        }
    }
}

fn main() {
    let mut word_count = DEFAULT_WORD_COUNT;
    let mut password_count = DEFAULT_PASSWORD_COUNT;

    if let Some(word_count_arg) = env::args().nth(1) {
        match parse_argument(word_count_arg) {
            Some(parsed_word_count) => word_count = parsed_word_count,
            None => {}
        }
    }

    if let Some(password_count_arg) = env::args().nth(2) {
        match parse_argument(password_count_arg) {
            Some(parsed_password_count) => password_count = parsed_password_count,
            None => {}
        }
    }

    let word_list: Vec<_> = WORDS.split("\n").collect();
    let mut rng = thread_rng();

    println!("Password Candidates: ");
    println!("---------------------");
    for _ in 0..password_count {
        let password = generate_password(&word_list, &mut rng, word_count);
        println!("> {}", password);
    }
}
