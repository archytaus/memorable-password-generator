# Memorable Password Generator

## Deploy

```sh
./bin/deploy
```

## Running

```sh
passwords <word count> <number of passwords>
```

**Example 1:**

```
ryanscott@Ryans-MacBook-Air:~$ passwords
Password Candidates:
---------------------
> sense-danger-whale-red-slightly
> do-growth-describe-case-forest
> base-thousand-curious-business-does
> especially-brown-able-now-bicycle
> fell-went-right-whatever-score
```

**Example 2:**

```
ryanscott@Ryans-MacBook-Air:~$ passwords 5 3
Password Candidates:
---------------------
> dress-gulf-solar-dollar-alone
> sunlight-drove-nodded-frighten-fairly
> flower-seat-comfortable-row-kids
```

**Example 3:**

```
ryanscott@Ryans-MacBook-Air:~$ passwords 2 4
Password Candidates:
---------------------
> wore-fort
> log-previous
> ring-provide
> salmon-last
```
